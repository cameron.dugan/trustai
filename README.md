# GameOfTrustAI

Using the rust language to make a bot that plays a game of trust.

Goal of the game: walk away with a score not equal to 0

Two buttons: t and f
* t: try to steal for double points
* f: trust the opponent to not steal

Outcomes: 
t/t: bot + 0, you + 0
t/f: bot + 2, you + 0
f/t: bot + 0, you + 2
f/f: bot + 2, you + 2

The ai's goal:
    beat you until you agree to trade

The ai starts off every game with zero knowledge of how the game works. (some reason always starts by hitting t)
The ai is incentivized to not win by spamming t, instead it only is satisfied if it wins with you by teaching you to spam f

This was an excuse for me to learn rust, and very basic neural networks.