use std::io;

fn main() {
    const NODES: usize = 5;
    let mut game = Trust{p1:0,p2:0,prev:vec![],max_size:10};

    let mut input_layer = vec![Node{dweight:0.0,is_input:true,bias:0.0,val:0.0,prev_layer:vec![],weights:vec![]};game.max_size * 2];
    let mut layer1 = vec![Node{dweight:0.0,is_input:false,bias:0.0,val:0.0,prev_layer:input_layer.clone(),weights: vec![1.0;input_layer.len()]};NODES];
    let mut layer2 = vec![Node{dweight:0.0,is_input:false,bias:0.0,val:0.0,prev_layer:layer1.clone(),weights:vec![1.0;layer1.len()]};2];
    let mut output= vec![Node{dweight:0.0,is_input:false,bias:0.0,val:0.0,prev_layer:layer2.clone(),weights:vec![1.0;layer2.len()]};1];
    let mut adlayer1 = vec![Node{dweight:0.0,is_input:false,bias:0.0,val:0.0,prev_layer:input_layer.clone(),weights: vec![1.0;input_layer.len()]};NODES];
    let mut adlayer2 = vec![Node{dweight:0.0,is_input:false,bias:0.0,val:0.0,prev_layer:layer1.clone(),weights:vec![1.0;layer1.len()]};2];
    let mut adoutput= vec![Node{dweight:0.0,is_input:false,bias:0.0,val:0.0,prev_layer:layer2.clone(),weights:vec![1.0;layer2.len()]};1];

    let mut i:i128 = 0;
    let mut uinput = String::new();
    let mut human;
    loop {
        println!("PvAi -> (y) or (n) -> AivAi battle?: ");
        io::stdin()
            .read_line(&mut uinput)
            .expect("Failed to read input");
        uinput = uinput.to_ascii_lowercase();
        human = uinput.starts_with('y');
        if human || uinput.starts_with("n") {break}
    }
    loop {
        if i == 10000 {break}
        let outnode1 = get_output(output.clone());
        let choice1 = outnode1 >= 0.5;
        let mut number = String::new();
        if human {
            println!("(t)ake or rest(any key)");
            io::stdin()
                .read_line(&mut number)
                .expect("Failed to read input");
            game.play(choice1,number.eq("t\n"))
        } else {
            let outnode2 = get_output(adoutput.clone());
            let choice2 = outnode2 >= 0.5;
            game.play(choice1,choice2);
        }
        if i>=2{
            let mut loss_val = loss(true,game.prev[game.prev.len()-1],outnode1);
            let dsum = sig_prime(outnode1) * loss_val;
            //println!("dsum: {}, loss: {}",dsum, loss_val);
            //println!("propogationfor output: ");
            output = propogate(dsum,output.clone());
            //println!("propogationfor layer1: ");
            layer2 = propogate(dsum,layer2.clone());
            //println!("propogationfor layer2: ");
            layer1 = propogate(dsum,layer1.clone());
            if !human{
                loss_val = loss(false,game.prev[game.prev.len()-1],outnode1);
                let dsum = sig_prime(outnode1) * loss_val;
                //println!("dsum2: {}, loss2: {}",dsum, loss_val);
                //println!("output propogating");
                adoutput = propogate(dsum,adoutput.clone());
                //println!("layer2 propogating");
                adlayer2 = propogate(dsum,adlayer2.clone());
                //println!("layer1 propogating");
                adlayer1 = propogate(dsum,adlayer1.clone());
                //println!("propogating complete");
            }
        }
        input_layer = update_input(game.clone(), input_layer);
        i += 1;
        let opponent;
        if human {opponent = "human"}
        else {opponent = "computer2"}
        println!("computer1: {}, {}: {}",game.p1,opponent,game.p2);
    }
}

fn propogate(mut cost:f32,output_l:Vec<Node>) -> Vec<Node> {
    let mut prop_out = output_l.clone();
    let mut i = 0;
    loop {
        if i == output_l.len() {break}
        let mut weights = prop_out[i].weights.clone();
        if prop_out[i].dweight != 0.0 {
            //println!("{}",prop_out[i].dweight);
            cost = prop_out[i].dweight;
            prop_out[i].dweight = 0.0;
        }
        let cost = vec![cost;weights.len()];
        let mut k = 0;
        loop {
            if k == weights.len() {break}
            let mut prevl = prop_out[i].prev_layer[k].clone();
            weights[k] = weights[k] + (cost[k] / prevl.clone().output());
            prevl.dweight = cost[k] / prevl.clone().output();
            prop_out[i].prev_layer[k] = prevl.clone();
            k+=1;
        }
        prop_out.get_mut(i).unwrap().weights = weights;
        i += 1;
    }
    return prop_out;
}

fn update_input(game:Trust,input_layer:Vec<Node>) -> Vec<Node>{
    let mut il = input_layer.clone();
    let scores = game.prev;
    let mut i = 0;
    for val in scores{
        if i >= input_layer.len() {break}
        il[i].bias = val.0 as f32;
        il[i+1].bias = val.1 as f32;
        i += 2;
    }
    return il;
}

fn sig_prime(v:f32) -> f32 {2.0*sigmoid(v)*(1.0-sigmoid(v))}

fn get_output(output_l:Vec<Node>) -> f32 {
    let mut output = 0.0;
    for node in output_l{
        output = node.output();
    }
    return output;
}

fn sigmoid(v:f32) -> f32 {
    const E: f32 = 2.71828;
    return 1.0/(1.0+f32::powf(E,-v))
}

//below .5 is trust above .5 is take
fn loss(p:bool,score:(i16,i16),this_output:f32) -> f32 {
    let target:f32;
    let my_score;
    let your_score;
    if p {
        my_score = score.0 ;
        your_score = score.1;
    } else {
        your_score = score.0;
        my_score = score.1;
    }
    //println!("myscore: {}, yourscore: {}",my_score,your_score);
    if my_score == 0 && your_score == 2 {target = 1.0}
    else if my_score == 0 {target = 0.5}
    else if my_score == 2 {target = 0.0} 
    else {target = 0.0}
    let mut sign:f32 = 1.0;
    if f32::is_sign_negative(target - this_output) {sign = -1.0}
    return sign * cost(target,this_output);
}

fn cost(target:f32,actual:f32) -> f32 {f32::powf(target - actual as f32,2.)}

#[derive(Clone,Debug)]
struct Node{
    is_input:bool,
    val:f32,
    bias:f32,
    dweight:f32,
    prev_layer: Vec<Node>,
    weights: Vec<f32>,
}
impl Node {
    fn output(self) -> f32 {
        if self.is_input {return self.bias}
        let mut sum: f32 = 0.0;
        let mut i = 0;
        for node in self.prev_layer{
            sum += node.output() * self.weights[i];
            i += 1;
        }
        sum += self.bias;
        sum = sigmoid(sum);
        return sum;
    }
}

#[derive(Clone)]
struct Trust{
    p1: i16,
    p2: i16,
    prev: Vec<(i16,i16)>,
    max_size: usize,
}
impl Trust{
    fn play(&mut self, p1d:bool, p2d:bool){
        let score: i16 = 2;
        let mut tmp: (i16,i16) = (0,0);
        if p1d & !p2d{
            self.p1 += score;
            tmp.0 += score;
        } else if p2d & !p1d {
            self.p2 += score;
            tmp.1 += score;
        } else if !p1d & !p2d {
            let s = score*2;
            self.p1 += s;
            self.p2 += s;
            tmp.0 += s;
            tmp.1 += s;
        }
        self.prev.append(&mut vec![tmp]);
        if self.prev.len() > self.max_size {let _e = self.prev.remove(0);}
    }
    fn _reset(&mut self){
        self.p1 = 0;
        self.p2 = 0;
    }
}